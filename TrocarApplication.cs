﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;
using System;
using System.IO;

namespace Trocar.ConsoleHarness
{
    public abstract class TrocarApplication
    {
        private IConfigurationRoot _configuration;
        private ILogger<TrocarApplication> _logger;
        private IServiceCollection _sc;

        private void InitializeServiceCollection()
        {
            _sc = new ServiceCollection();
            _sc.AddSingleton<IConfigurationRoot>(_configuration);
        }
        private void InitializeLogger()
        {
            // Add access to generic IConfigurationRoot
            LogManager.Configuration = new NLogLoggingConfiguration(_configuration.GetSection("NLog"));


            _sc.AddOptions();
            _sc.AddLogging(lb =>
            {
                lb.ClearProviders();
//                lb.AddConfiguration(_configuration);
                lb.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                lb.AddNLog(_configuration);

            });

        }
        private void BuildConfiguration()
        {
            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appSettings.json", false)
                .Build();
        }
        private void ConfigureServices(string[] args)
        {
            BuildConfiguration();
            InitializeServiceCollection();
            InitializeLogger();
        }
        public void Start(string[] args)
        {
            ConfigureServices(args);
            onConfigureServices(args, _sc, _configuration);
            var sp = _sc.BuildServiceProvider();
            var logger = LogManager.GetCurrentClassLogger();
            _logger = sp.GetService<ILogger<TrocarApplication>>();
            _logger.LogTrace("---Starting---");
            onStart(sp);
            _logger.LogTrace("---End Starting---");
        }

        protected virtual void onConfigureServices(string[] args, IServiceCollection sc) => onConfigureServices(args, sc, _configuration);
        protected virtual void onConfigureServices(string[] args, IServiceCollection sc, IConfiguration configuration) { }
        protected virtual void onStart(ServiceProvider sp) { }

    }
}
