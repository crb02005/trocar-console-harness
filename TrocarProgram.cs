﻿namespace Trocar.ConsoleHarness
{
    public class TrocarProgram<TApplication>
        where TApplication: TrocarApplication, new()
    {
        public static void Main(string[] args)
        {
            TApplication ta = new TApplication();
            ta.Start(args);
        }
    }
}
