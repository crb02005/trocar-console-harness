﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Trocar.ConsoleHarness
{
    public class HelloWorldApplication: TrocarApplication
    {
        protected override void onConfigureServices(string[] args, IServiceCollection sc) { }
        protected override void onStart(ServiceProvider sp) {

            Console.WriteLine("Hello World.");
        
        }

    }
}
