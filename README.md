# TROCAR Console App Starter Kit

NLog is configured out of the box to use appSettings.json

Just make your Program.cs inherit: TrocarProgram<YourApplicationWhichInheritsTrocarApplication>

example Program.cs
```csharp
    class Program: Trocar.ConsoleHarness.TrocarProgram<SuperApp>
    {
        static void Main(string[] args)
        {
            TrocarProgram<SuperApp>.Main(args);
        }
    }
```

example App.cs
```csharp
   public class SuperApp: Trocar.ConsoleHarness.TrocarApplication
    {
        protected override void onConfigureServices(string[] args, IServiceCollection sc)
        {
            base.onConfigureServices(args, sc);
        }
        protected override void onStart(ServiceProvider sp)
        {
            base.onStart(sp);
            Console.WriteLine("here");
        }
    }
```

For logging and configuration:

Create an _appSettings.json_ file, and set properties to copy always.

It should be JSON and should contain an "NLog" key with a value.

example which logs to the console and a file
```js
{
    "NLog": {
    "throwConfigExceptions": true,
    "rules": [
      {
        "logger": "*",
        "minLevel": "Information",
        "writeTo": "logconsole"
      },
      {
        "logger": "*",
        "minLevel": "Information",
        "writeTo": "logFile"
      }
    ],
    "targets": {
      "logfile": {
        "type": "File",
        "fileName": "C:\\logs\\console_harness.txt",
        "deleteOldFileOnStartup": true
      },
      "logconsole": {
        "type": "Console"
      }
    }
  }
}
```

When you run your project, the console should show logging, and there should be a file that is created and overwritten in C:\logs\

